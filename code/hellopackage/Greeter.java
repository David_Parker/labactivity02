package hellopackage;
import java.util.Scanner;
import java.util.Random;
import secondpackage.Utilities;

public class Greeter {
    public static void main (String[] args) {
        Scanner reader = new Scanner(System.in);
        Utilities util = new Utilities();
        int num = 0;
        System.out.println("Please enter an integer");
        num = reader.nextInt();
        System.out.println(util.doubleMe(num));
    }
}